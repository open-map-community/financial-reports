# Donations

OMC is a 100% free service that relies on its members choosing to donate to help cover OMC's operating expenses. You can directly help OMC improve and continue to exist by donating.

Your donations will only be spent on things directly related to OMC. All of OMC's finances are transparent and fully available in this repository.

OMC currently pays for:

- Domain names (mapcommunity.org)
- Servers

## How to Get Cryptocurrency & Donate

A friend told me they buy Monero with a credit card from https://www.kraken.com/ or https://www.binance.com/en .

But I prefer to buy with cash, so this is how I do it:

1. Download Monero XMR wallet for PC: https://www.getmonero.org/
2. Download Ethereum ETH wallet for mobile: https://www.exodus.com/
3. Find the nearest ATM, and pay cash to buy Ethereum ETH: https://coinatmradar.com/
4. Trade Ethereum ETH for Monero XMR: https://trocador.app/en/
5. Donate to OMCs Monero XMR address:
862rWgo6e8UH8haKevP1Tx11d5grzxsmsYJw3XF5SBzZhiGWekXJnyACJDCVCr7BfZcye9cpvbmzgHqE59BkLf1u3JgkKM7

## Rewards for Donating

Anyone who donates is eligible for a "Donator" role in OMC Chat. Those who donate more than a month's worth of expenses are also eligible for a custom role of their choosing. The specifics of these rewards can be seen here: https://mapcommunity.org/#tos-donations

# Financial Reports

Currency info:
* XMR = Monero
* BTC = Bitcoin
* BCH = Bitcoin Cash
* DASH = Dash
* ETH = Ethereum
* LTC = Litecoin
* ZEC = Zcash
* EUR = Euro
* USD = United States Dollar

## 2021

### 2021-4
* Total Donations: + **0.0899** XMR
* Transfers: - **0.087791** XMR - **0.00001025** XMR fee (for) VPS Provider

### 2021-5
* Total Donations: + 0 XMR
* Transfers: - 0 XMR

### 2021-6
* Total Donations: + **1.1998663625** XMR
* Transfers: **0.14346** XMR - **0.00000959** XMR fee (for) VPS Provider
* Notes: OMC received a very generous donation this month, and will be using it to upgrade OMC's infrastructure with the aim of improving reliability and uptime.

### 2021-7
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|         **0.9463961125** XMR | + **0.377308094323** XMR |              - **0.56791** XMR - **0.00000926** XMR fee (for) + **75** EUR provider1 |     = **0.867893946823** XMR |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|         **30** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |         **75** EUR provider1 |

### 2021-8
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **0.867893946823** XMR |                          |                                                                                      |     = **0.867893946823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **75** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **45** EUR provider1 |

### 2021-9
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **0.867893946823** XMR |                          |                                                                                      |     = **0.867893946823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **45** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **15** EUR provider1 |

### 2021-10
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **0.867893946823** XMR |                          |           - **0.18381075** XMR - **0.00001075** XMR fee (for) + **45** EUR provider1 |        = **0.684072447** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **15** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **30** EUR provider1 |

### 2021-11
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|          **0.684072447** XMR |                          |           - **0.28590777** XMR - **0.00000758** XMR fee (for) + **60** EUR provider1 |     = **0.398157096823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **30** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |

### 2021-12
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **0.398157096823** XMR |         + **0.9999** XMR |                                                                                      |     = **1.398057096823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **30** EUR provider1 |

## 2022

### 2022-1
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.398057096823** XMR |              + **2** XMR |           - **0.13800726** XMR - **0.00000981** XMR fee (for) + **30** EUR provider1 |     = **2.836792966823** XMR |
|                              |                          |              - **0.42324** XMR - **0.00000706** XMR fee (for) + **75** EUR provider1 |                              |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **30** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **90** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-2
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.836792966823** XMR |                          |                                                                                      |     = **2.836792966823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **90** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **45** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-3
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.836792966823** XMR |                          |              - **0.28455** XMR - **0.00000667** XMR fee (for) + **45** EUR provider1 |     = **2.552236296823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **45** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **30** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Year domain  |                              |

### 2022-4
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.552236296823** XMR |                          |               - **0.3815** XMR - **0.00000866** XMR fee (for) + **75** EUR provider1 |     = **2.170727636823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **30** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-5
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.170727636823** XMR |                          |                - **0.2286** XMR - **0.0000083** XMR fee (for) + **45** EUR provider1 |     = **1.942119336823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-6
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.942119336823** XMR |                          |               - **0.2436** XMR - **0.00000795** XMR fee (for) + **45** EUR provider1 |     = **1.698511386823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-7
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.698511386823** XMR |                          |               - **0.4086** XMR - **0.00000583** XMR fee (for) + **45** EUR provider1 |     = **1.289905556823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-8
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.289905556823** XMR |                          |                - **0.296** XMR - **0.00000583** XMR fee (for) + **45** EUR provider1 |     = **0.993899726823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-9
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **0.993899726823** XMR |                          |                - **0.295** XMR - **0.00003078** XMR fee (for) + **45** EUR provider1 |     = **0.698868946823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-10
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **0.698868946823** XMR |           + **0.02** XMR |               - **0.2961** XMR - **0.00003068** XMR fee (for) + **45** EUR provider1 |     = **0.422738266823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-11
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **0.422738266823** XMR |           + **4.33** XMR |                - **0.298** XMR - **0.00003072** XMR fee (for) + **45** EUR provider1 |     = **4.454707546823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2022-12
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **4.454707546823** XMR |                          |               - **0.3324** XMR - **0.00004436** XMR fee (for) + **45** EUR provider1 |     = **4.122263186823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

## 2023

### 2023-1
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **4.122263186823** XMR |                          |               - **0.3561** XMR - **0.00004432** XMR fee (for) + **45** EUR provider1 |     = **3.766118866823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2023-2
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.766118866823** XMR |                          |               - **0.3092** XMR - **0.00004446** XMR fee (for) + **45** EUR provider1 |     = **3.456874406823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Month proxy  |                              |

### 2023-3
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.456874406823** XMR |                          |               - **0.3474** XMR - **0.00004446** XMR fee (for) + **45** EUR provider1 |     = **3.109429946823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |
|                              |                          |                                    - **15** EUR provider1 (for) + **1** Year domain  |                              |

### 2023-4
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.109429946823** XMR |                          |               - **0.2309** XMR - **0.00003072** XMR fee (for) + **30** EUR provider1 |     = **2.878499226823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **60** EUR provider1 |

### 2023-5
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.878499226823** XMR |                          |               - **0.1182** XMR - **0.00003066** XMR fee (for) + **15** EUR provider1 |     = **2.760268566823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |        + **0.08109** ETH |                                                                                      |            = **0.08109** ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **60** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **45** EUR provider1 |

### 2023-6
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.760268566823** XMR |                          |               - **0.2436** XMR - **0.00003072** XMR fee (for) + **30** EUR provider1 |     = **2.516637846823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|                        0 LTC |       + **1.128869** LTC |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **45** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **45** EUR provider1 |

### 2023-7
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.516637846823** XMR |                          |               - **0.2199** XMR - **0.00003066** XMR fee (for) + **30** EUR provider1 |     = **2.296707186823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **45** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **45** EUR provider1 |

* provider1 temporarily suspended services.

### 2023-8
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **2.296707186823** XMR |                          |              - **0.22775** XMR - **0.00003066** XMR fee (for) + **30** EUR provider1 |     = **1.960595186823** XMR |
|                              |                          |           - **0.10830068** XMR - **0.00003066** XMR fee (for) + **30** EUR provider1 |                              |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|         **45** EUR provider1 |                          |                                    - **30** EUR provider1 (for) + **1** Month server |       = **75** EUR provider1 |

* provider1 permanently suspended services.

### 2023-9
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.960595186823** XMR |                          |                                                                                      |     = **1.960595186823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2023-10
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.960595186823** XMR |                          |                                                                                      |     = **1.960595186823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2023-11
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.960595186823** XMR |                          |                                                                                      |     = **1.960595186823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2023-12
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.960595186823** XMR |                          |                                                                                      |     = **1.960595186823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

## 2024

### 2024-1
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.960595186823** XMR |                          |                                                                                      |     = **1.960595186823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-2
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.960595186823** XMR |                          |           - **0.21653701** XMR - **0.00003066** XMR fee (for) + **27** USD provider3 |     = **1.744027516823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |
|      **18.85** USD provider3 |                          |                             - **26.70** USD provider3 (for) + **1** Month rocketchat |    = **19.15** USD provider3 |

* set up new account with provider3.

### 2024-3
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.744027516823** XMR |                          |                                                                                      |     = **1.744027516823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |
|  ~~**19.15** USD provider3~~ |                          |                                                                                      |                              |

* closed account with provider3.

### 2024-4
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.744027516823** XMR |                          |                                                                                      |     = **1.744027516823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-5
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.744027516823** XMR |                          |                                                                                      |     = **1.744027516823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-6
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.744027516823** XMR |                          |                                                                                      |     = **1.744027516823** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|              **0.08109** ETH |                          |                                                                                      |            = **0.08109** ETH |
|             **1.128869** LTC |                          |                                                                                      |           = **1.128869** LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-7
|             Starting Balance |          Total Donations |                                                                                  Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------------:|-----------------------------:|
|       **1.744027516823** XMR |                          | - **0.108448114731** XMR - **0.00012288** XMR fee (for) + **17.29** USD provider2 (domain) |     = **3.691043844458** XMR |
|                              |                          | - **0.128107897634** XMR - **0.00003058** XMR fee (for) + **20.00** USD provider2 (server) |                              |
|                        0 BTC |                          |                                                                                            |                      = 0 BTC |
|                        0 BCH |                          |                                                                                            |                      = 0 BCH |
|                       0 DASH |                          |                                                                                            |                     = 0 DASH |
|              **0.08109** ETH |                          |               - **0.08091383** ETH - **0.00017617** ETH fee (for) + **1.68758518** XMR OMC |                      = 0 ETH |
|             **1.128869** LTC |                          |                  - **1.1288594** LTC - **0.0000096** LTC fee (for) + **0.4961408** XMR OMC |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                            |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                            |   ~~= **75** EUR provider1~~ |

### 2024-8
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.691043844458** XMR |                          |                                                                                      |     = **3.691043844458** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-9
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.691043844458** XMR |                          |                                                                                      |     = **3.691043844458** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-10
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.691043844458** XMR |                          |                                                                                      |     = **3.691043844458** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-11
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.691043844458** XMR |                          |                                                                                      |     = **3.691043844458** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2024-12
|             Starting Balance |          Total Donations |                                                                            Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.691043844458** XMR |                          |                                                                                      |     = **3.691043844458** XMR |
|                        0 BTC |                          |                                                                                      |                      = 0 BTC |
|                        0 BCH |                          |                                                                                      |                      = 0 BCH |
|                       0 DASH |                          |                                                                                      |                     = 0 DASH |
|                        0 ETH |                          |                                                                                      |                      = 0 ETH |
|                        0 LTC |                          |                                                                                      |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                      |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                      |   ~~= **75** EUR provider1~~ |

### 2025-1
|             Starting Balance |          Total Donations |                                                                                  Transfers |               Ending Balance |
|-----------------------------:|-------------------------:|-------------------------------------------------------------------------------------------:|-----------------------------:|
|       **3.691043844458** XMR |                          | - **0.104326488097** XMR - **0.00004462** XMR fee (for) + **20.00** USD provider2 (server) |     = **3.586642032561** XMR |
|                              |                          |     - **0.0000000038** XMR - **0.0000307** XMR fee (for) + **0.00** USD provider2 (server) |                              |
|                        0 BTC |                          |                                                                                            |                      = 0 BTC |
|                        0 BCH |                          |                                                                                            |                      = 0 BCH |
|                       0 DASH |                          |                                                                                            |                     = 0 DASH |
|                        0 ETH |                          |                                                                                            |                      = 0 ETH |
|                        0 LTC |                          |                                                                                            |                      = 0 LTC |
|                        0 ZEC |                          |                                                                                            |                      = 0 ZEC |
|     ~~**75** EUR provider1~~ |                          |                                                                                            |   ~~= **75** EUR provider1~~ |

* required - **17.29** USD provider2 (by 2026-3March-22nd for) + domain **1** Year.
* required - **20.00** USD provider2 (by 2025-7July-17th for) + server **6** Month.
* required - **26.70** USD provider3 (by 2025-02February-7th for) + rocketchat **30** Day.